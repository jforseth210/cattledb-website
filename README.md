# CattleDB Landing Page

This is the source code for [https://cattledb.jforseth.com](https://cattledb.jforseth.com). It's based off of a template from [https://bootstrap.build](https://bootstrap.build/templates). For more information about CattleDB please visit [https://cattledb.jforseth.com](https://cattledb.jforseth.com).
